<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20240410172551 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('CREATE SEQUENCE predictions_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE sport_events_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE teams_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE users_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE predictions (id INT NOT NULL, user_id INT NOT NULL, sport_event_id INT NOT NULL, prediction VARCHAR(1) NOT NULL, goals INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_8E87BCE6A76ED395 ON predictions (user_id)');
        $this->addSql('CREATE INDEX IDX_8E87BCE647551731 ON predictions (sport_event_id)');
        $this->addSql('CREATE TABLE sport_events (id INT NOT NULL, home_team_id INT NOT NULL, away_team_id INT NOT NULL, name VARCHAR(40) NOT NULL, kick_off_time TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_375883EB9C4C13F6 ON sport_events (home_team_id)');
        $this->addSql('CREATE INDEX IDX_375883EB45185D02 ON sport_events (away_team_id)');
        $this->addSql('CREATE TABLE teams (id INT NOT NULL, name VARCHAR(40) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE users (id INT NOT NULL, name VARCHAR(40) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('ALTER TABLE predictions ADD CONSTRAINT FK_8E87BCE6A76ED395 FOREIGN KEY (user_id) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE predictions ADD CONSTRAINT FK_8E87BCE647551731 FOREIGN KEY (sport_event_id) REFERENCES sport_events (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE sport_events ADD CONSTRAINT FK_375883EB9C4C13F6 FOREIGN KEY (home_team_id) REFERENCES teams (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE sport_events ADD CONSTRAINT FK_375883EB45185D02 FOREIGN KEY (away_team_id) REFERENCES teams (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP SEQUENCE predictions_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE sport_events_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE teams_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE users_id_seq CASCADE');
        $this->addSql('ALTER TABLE predictions DROP CONSTRAINT FK_8E87BCE6A76ED395');
        $this->addSql('ALTER TABLE predictions DROP CONSTRAINT FK_8E87BCE647551731');
        $this->addSql('ALTER TABLE sport_events DROP CONSTRAINT FK_375883EB9C4C13F6');
        $this->addSql('ALTER TABLE sport_events DROP CONSTRAINT FK_375883EB45185D02');
        $this->addSql('DROP TABLE predictions');
        $this->addSql('DROP TABLE sport_events');
        $this->addSql('DROP TABLE teams');
        $this->addSql('DROP TABLE users');
    }
}
