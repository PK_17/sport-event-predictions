<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Prediction;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class PredictionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Prediction::class);
    }

    public function findBySportEventIdAndUserId(int $sportEventId, int $userId): ?Prediction
    {
        return $this->createQueryBuilder('p')
            ->andWhere('IDENTITY(p.sportEvent) = :sportEventId')
            ->andWhere('IDENTITY(p.user) = :userId')
            ->setParameter('sportEventId', $sportEventId)
            ->setParameter('userId', $userId)
            ->getQuery()
            ->getOneOrNullResult();
    }
}
