<?php

declare(strict_types=1);

namespace App\Factory;

use App\App\Prediction\Command\PatchPrediction\DTO\PatchPredictionDTO;
use App\App\Prediction\Command\PostPrediction\DTO\PostPredictionDTO;
use App\Entity\Prediction;
use App\Entity\SportEvent;
use App\Entity\User;

class PredictionFactory
{
    public function create(PostPredictionDTO $dto, SportEvent $sportEvent, User $user): Prediction
    {
        return (new Prediction())
            ->setSportEvent($sportEvent)
            ->setUser($user)
            ->setPrediction($dto->prediction)
            ->setGoals($dto->goals);
    }

    public function update(PatchPredictionDTO $dto, Prediction $prediction): Prediction
    {
        return $prediction
            ->setPrediction($dto->prediction)
            ->setGoals($dto->goals);
    }
}
