<?php

declare(strict_types=1);

namespace App\Event;

use App\DTO\ApiResponse;
use App\Handler\ApiResponseHandler;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Messenger\Exception\HandlerFailedException;
use Symfony\Component\Serializer\SerializerInterface;
use Throwable;
use Exception;
use ErrorException;
use ParseError;
use TypeError;

readonly class ExceptionSubscriber implements EventSubscriberInterface
{
    public function __construct(
        private ApiResponseHandler $apiResponseHandler,
    ) {
    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::EXCEPTION => ['onKernelException', -127]
        ];
    }

    public function onKernelException(ExceptionEvent $event): void
    {
        $exception = $event->getThrowable();

        if ($exception instanceof HandlerFailedException) {
            $exception = $exception->getPrevious();
        }

        if (!$exception instanceof Exception) {
            $exception = $this->adapter($exception);
        }

        $response = $this->apiResponseHandler->createApiResponse(
            data: $exception->getMessage(),
            status: false,
            httpCode: $this->getHttpCode($exception),
        );

        $event->setResponse($response);
    }

    private function adapter(Throwable $e): ErrorException
    {
        if ($e instanceof ParseError) {
            $severity = E_PARSE;
        } elseif ($e instanceof TypeError) {
            $severity = E_RECOVERABLE_ERROR;
        } else {
            $severity = E_ERROR;
        }

        return new ErrorException(
            $e->getMessage(),
            $e->getCode(),
            $severity
        );
    }

    private function getHttpCode(Exception $exception): int
    {
        if ($exception instanceof NotFoundHttpException) {
            return Response::HTTP_NOT_FOUND;
        }

        if ($exception instanceof BadRequestHttpException) {
            return Response::HTTP_BAD_REQUEST;
        }

        if ($exception instanceof HttpExceptionInterface) {
            return Response::HTTP_BAD_REQUEST;
        }

        return Response::HTTP_INTERNAL_SERVER_ERROR;
    }
}
