<?php

declare(strict_types=1);

namespace App\Handler;

use App\DTO\ApiResponse;
use Symfony\Component\Serializer\SerializerInterface;

readonly class ApiResponseHandler
{
    private const SERIALIZATION_GROUPS = ['API'];

    public function __construct(
        private SerializerInterface $serializer,
    ) {
    }

    public function createApiResponse(
        mixed $data = null,
        bool $status = true,
        int $httpCode = 200,
        array $context = [],
    ): ApiResponse {
        if (is_string($data)) {
            $data = [
                'message' => $data,
            ];
        }

        $result['status'] = $status;
        $result['data'] = $data;
        $result = $this->serializer->serialize(
            $result,
            'json',
            array_merge_recursive(['groups' => self::SERIALIZATION_GROUPS], $context),
        );
        return new ApiResponse($result, $httpCode);
    }
}
