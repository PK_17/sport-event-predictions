<?php

declare(strict_types=1);

namespace App\Validator;

use App\App\Prediction\Command\PatchPrediction\DTO\PatchPredictionDTO;
use App\App\Prediction\Command\PostPrediction\DTO\PostPredictionDTO;
use App\Enum\PredictionEnum;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class ScoreValidator extends ConstraintValidator
{
    public function validate(mixed $value, Constraint $constraint): void
    {
        if (!($value instanceof PostPredictionDTO) && !($value instanceof PatchPredictionDTO)) {
            return;
        }

        /** @var PostPredictionDTO|PatchPredictionDTO $value */
        if (PredictionEnum::DRAW->value === $value->prediction && !$this->areGoalsEven($value->goals)) {
            /** @var Score $constraint */
            $this->context
                ->buildViolation($constraint->message)
                ->atPath('goals')
                ->addViolation();

            return;
        }

        if (
            in_array($value->prediction, [PredictionEnum::HOME_WIN->value, PredictionEnum::AWAY_WIN->value], true)
            && !$this->areGoalsOdd($value->goals)
        ) {
            /** @var Score $constraint */
            $this->context
                ->buildViolation($constraint->message)
                ->atPath('goals')
                ->addViolation();

            return;
        }
    }

    private function areGoalsEven(?int $goals): bool
    {
        return null !== $goals && 0 === $goals % 2;
    }

    private function areGoalsOdd(?int $goals): bool
    {
        return null !== $goals && 0 !== $goals % 2;
    }
}
