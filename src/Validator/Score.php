<?php

declare(strict_types=1);

namespace App\Validator;

use Symfony\Component\Validator\Constraint;
use Attribute;

#[Attribute(Attribute::TARGET_CLASS)]
class Score extends Constraint
{
    public string $message = 'Draw prediction can only have an even number of goals and win prediction can only have an odd number of goals.';

    public function getTargets(): string
    {
        return self::CLASS_CONSTRAINT;
    }

    public function validatedBy(): string
    {
        return ScoreValidator::class;
    }
}