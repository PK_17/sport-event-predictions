<?php

declare(strict_types=1);

namespace App\Validator;

use App\DTO\RequestDTO;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\PropertyInfo\PropertyTypeExtractorInterface;
use Symfony\Component\Validator\ConstraintViolationInterface;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use LogicException;

readonly class RequestValidator
{
    public function __construct(
        private ValidatorInterface $validator,
        private PropertyTypeExtractorInterface $typeExtractor,
    ) {
    }

    public function validate(Request $request, string $dtoClass): RequestDTO
    {
        $dto = $this->hydrateDTO($request, $dtoClass);
        $violations = $this->validator->validate($dto);

        if (count($violations) > 0) {
            $message = $this->prepareErrorMessage($violations);
            throw new BadRequestHttpException($message);
        }

        return $dto;
    }

    private function hydrateDTO(Request $request, string $dtoClass): RequestDTO
    {
        $dto = new $dtoClass();
        $requestContent = json_decode($request->getContent(), true);

        foreach ($requestContent as $property => $value) {
            if (property_exists($dto, $property)) {
                $this->validateValueType($dto, (string) $property, $value);
                $dto->$property = $value;
            }
        }

        return $dto;
    }

    private function validateValueType(RequestDTO $dto, string $property, mixed $value): void
    {
        $propertyTypes = $this->typeExtractor->getTypes($dto::class, $property);
        $propertyType = $propertyTypes[0] ?? null;
        if (!$propertyType) {
            $message = sprintf('Property %s has no defined type!', $property);
            throw new LogicException($message);
        }

        $propertyType = $propertyType->getBuiltinType();
        $valueType = $this->adaptType(gettype($value));
        if ($propertyType !== $valueType) {
            $message = sprintf('Property %s type is %s - %s given', $property, $propertyType, $valueType);
            throw new BadRequestHttpException($message);
        }
    }

    private function prepareErrorMessage(ConstraintViolationListInterface $violations): string
    {
        $messages = [];
        /** @var ConstraintViolationInterface $violation */
        foreach ($violations as $violation) {
            $messages[] = sprintf(
                'At property: %s - %s',
                $violation->getPropertyPath(),
                $violation->getMessage(),
            );
        }

        return implode('; ', $messages);
    }

    private function adaptType(string $type): string
    {
        return match ($type) {
            'integer' => 'int',
            'boolean' => 'bool',
            default => $type,
        };
    }
}
