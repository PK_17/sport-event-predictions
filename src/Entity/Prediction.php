<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\PredictionRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Table(name: 'predictions')]
#[ORM\UniqueConstraint(columns: ['sport_event_id', 'user_id'])]
#[ORM\Entity(repositoryClass: PredictionRepository::class)]
class Prediction
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: Types::INTEGER, nullable: false, options: ['unsigned' => true])]
    #[Groups(groups: ['API'])]
    private int $id;

    #[ORM\ManyToOne(targetEntity: User::class)]
    #[ORM\JoinColumn(name: 'user_id', referencedColumnName: 'id', nullable: false)]
    #[Groups(groups: ['API'])]
    private User $user;

    #[ORM\ManyToOne(targetEntity: SportEvent::class)]
    #[ORM\JoinColumn(name: 'sport_event_id', referencedColumnName: 'id', nullable: false)]
    #[Groups(groups: ['API'])]
    private SportEvent $sportEvent;

    #[ORM\Column(type: Types::STRING, length: 1, nullable: false)]
    #[Groups(groups: ['API'])]
    private string $prediction;

    #[ORM\Column(type: Types::INTEGER, nullable: false)]
    #[Groups(groups: ['API'])]
    private int $goals;

    public function getId(): int
    {
        return $this->id;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;
        return $this;
    }

    public function getSportEvent(): SportEvent
    {
        return $this->sportEvent;
    }

    public function setSportEvent(SportEvent $sportEvent): self
    {
        $this->sportEvent = $sportEvent;
        return $this;
    }

    public function getPrediction(): string
    {
        return $this->prediction;
    }

    public function setPrediction(string $prediction): self
    {
        $this->prediction = $prediction;
        return $this;
    }

    public function getGoals(): int
    {
        return $this->goals;
    }

    public function setGoals(int $goals): self
    {
        $this->goals = $goals;
        return $this;
    }
}