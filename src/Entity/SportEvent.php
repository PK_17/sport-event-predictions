<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\SportEventRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use DateTime;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Table(name: 'sport_events')]
#[ORM\Entity(repositoryClass: SportEventRepository::class)]
class SportEvent
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: Types::INTEGER, nullable: false, options: ['unsigned' => true])]
    #[Groups(groups: ['API'])]
    private int $id;

    #[ORM\Column(type: Types::STRING, length: 40, nullable: false)]
    #[Groups(groups: ['API'])]
    private string $name;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: false)]
    #[Groups(groups: ['API'])]
    private DateTime $kickOffTime;

    #[ORM\ManyToOne(targetEntity: Team::class)]
    #[ORM\JoinColumn(name: 'home_team_id', referencedColumnName: 'id', nullable: false)]
    #[Groups(groups: ['API'])]
    private Team $homeTeam;

    #[ORM\ManyToOne(targetEntity: Team::class)]
    #[ORM\JoinColumn(name: 'away_team_id', referencedColumnName: 'id', nullable: false)]
    #[Groups(groups: ['API'])]
    private Team $awayTeam;

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    public function getKickOffTime(): DateTime
    {
        return $this->kickOffTime;
    }

    public function setKickOffTime(DateTime $kickOffTime): self
    {
        $this->kickOffTime = $kickOffTime;
        return $this;
    }

    public function getHomeTeam(): Team
    {
        return $this->homeTeam;
    }

    public function setHomeTeam(Team $homeTeam): self
    {
        $this->homeTeam = $homeTeam;
        return $this;
    }

    public function getAwayTeam(): Team
    {
        return $this->awayTeam;
    }

    public function setAwayTeam(Team $awayTeam): self
    {
        $this->awayTeam = $awayTeam;
        return $this;
    }
}
