<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Team;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class TeamFixtures extends Fixture
{
    public const TEAM_A_REF = 'team-a';
    public const TEAM_B_REF = 'team-b';
    public const TEAM_C_REF = 'team-c';
    public const TEAM_D_REF = 'team-d';

    private const TEAM_FIXTURES = [
        [
            'name' => 'TeamA',
            'ref' => self::TEAM_A_REF,
        ],
        [
            'name' => 'TeamB',
            'ref' => self::TEAM_B_REF,
        ],
        [
            'name' => 'TeamC',
            'ref' => self::TEAM_C_REF,
        ],
        [
            'name' => 'TeamD',
            'ref' => self::TEAM_D_REF,
        ],
    ];

    public function load(ObjectManager $manager): void
    {
        foreach (self::TEAM_FIXTURES as $teamFixture) {
            $team = (new Team())
                ->setName($teamFixture['name']);

            $manager->persist($team);
            $this->addReference($teamFixture['ref'], $team);
        }

        $manager->flush();
    }
}
