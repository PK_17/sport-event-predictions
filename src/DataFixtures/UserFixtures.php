<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class UserFixtures extends Fixture
{
    private const USER_FIXTURES = [
        [
            'name' => 'Jan Kowalski',
        ],
        [
            'name' => 'Zygmunt Wójcik',
        ],
        [
            'name' => 'Radosław Zieliński',
        ],
        [
            'name' => 'Alicja Nowak',
        ],
        [
            'name' => 'Tadeusz Wiśniewski',
        ],
        [
            'name' => 'Bogdan Lewandowski',
        ],
        [
            'name' => 'Stefania Szymańska',
        ],
        [
            'name' => 'Marianna Woźniak',
        ],
        [
            'name' => 'Anna Kowalczyk',
        ],
        [
            'name' => 'Arkadiusz Kamiński',
        ],
    ];

    public function load(ObjectManager $manager): void
    {
        foreach (self::USER_FIXTURES as $userFixture) {
            $user = (new User())
                ->setName($userFixture['name']);

            $manager->persist($user);
        }

        $manager->flush();
    }
}
