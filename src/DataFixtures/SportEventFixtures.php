<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\SportEvent;
use App\Entity\Team;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use DateTime;

class SportEventFixtures extends Fixture implements DependentFixtureInterface
{
    private const SPORT_EVENT_FIXTURES = [
        [
            'name' => 'TeamA vs TeamB, Stadion Śląski',
            'kickOffTime' => '2024-04-20 18:00:00',
            'homeTeam' => TeamFixtures::TEAM_A_REF,
            'awayTeam' => TeamFixtures::TEAM_B_REF,
        ],
        [
            'name' => 'TeamC vs TeamD, Baltic Arena',
            'kickOffTime' => '2024-04-20 21:00:00',
            'homeTeam' => TeamFixtures::TEAM_C_REF,
            'awayTeam' => TeamFixtures::TEAM_D_REF,
        ],
        [
            'name' => 'TeamD vs TeamA, Stadion Narodowy',
            'kickOffTime' => '2024-04-24 18:00:00',
            'homeTeam' => TeamFixtures::TEAM_D_REF,
            'awayTeam' => TeamFixtures::TEAM_A_REF,
        ],
        [
            'name' => 'TeamB vs TeamC, Baltic Arena',
            'kickOffTime' => '2024-04-24 21:00:00',
            'homeTeam' => TeamFixtures::TEAM_B_REF,
            'awayTeam' => TeamFixtures::TEAM_C_REF,
        ],
        [
            'name' => 'TeamB vs TeamD, Stadion Narodowy',
            'kickOffTime' => '2024-04-28 21:00:00',
            'homeTeam' => TeamFixtures::TEAM_B_REF,
            'awayTeam' => TeamFixtures::TEAM_D_REF,
        ],
        [
            'name' => 'TeamA vs TeamC, Stadion Śląski',
            'kickOffTime' => '2024-04-28 21:00:00',
            'homeTeam' => TeamFixtures::TEAM_A_REF,
            'awayTeam' => TeamFixtures::TEAM_C_REF,
        ],
    ];


    public function load(ObjectManager $manager): void
    {
        foreach (self::SPORT_EVENT_FIXTURES as $sportEventFixture) {
            $kickOffTime = new DateTime($sportEventFixture['kickOffTime']);
            $homeTeam = $this->getReference($sportEventFixture['homeTeam'], Team::class);
            $awayTeam = $this->getReference($sportEventFixture['awayTeam'], Team::class);

            $sportEvent = (new SportEvent())
                ->setName($sportEventFixture['name'])
                ->setKickOffTime($kickOffTime)
                ->setHomeTeam($homeTeam)
                ->setAwayTeam($awayTeam);

            $manager->persist($sportEvent);
        }

        $manager->flush();
    }

    public function getDependencies(): array
    {
        return [
            TeamFixtures::class,
        ];
    }
}
