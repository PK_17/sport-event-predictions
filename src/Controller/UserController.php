<?php

declare(strict_types=1);

namespace App\Controller;

use App\App\User\Query\GetUserPredictions\GetUserPredictionsQuery;
use App\App\User\Query\GetUsers\GetUsersQuery;
use App\DTO\ApiResponse;

class UserController extends ApiController
{
    public function getUsers(): ApiResponse
    {
        $query = new GetUsersQuery();
        return $this->apiResponseHandler->createApiResponse($this->handleMessage($query));
    }

    public function getUserPredictions(): ApiResponse
    {
        $query = new GetUserPredictionsQuery((int) $this->request->get('id'));
        return $this->apiResponseHandler->createApiResponse($this->handleMessage($query));
    }
}
