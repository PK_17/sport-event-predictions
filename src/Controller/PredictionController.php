<?php

declare(strict_types=1);

namespace App\Controller;

use App\App\Prediction\Command\DeletePrediction\DeletePredictionCommand;
use App\App\Prediction\Command\PatchPrediction\PatchPredictionCommand;
use App\App\Prediction\Command\PostPrediction\PostPredictionCommand;
use App\App\Prediction\Query\GetPrediction\GetPredictionQuery;
use App\App\Prediction\Query\GetPredictions\GetPredictionsQuery;
use App\DTO\ApiResponse;
use Symfony\Component\HttpFoundation\Response;

class PredictionController extends ApiController
{
    public function getPredictions(): ApiResponse
    {
        $query = new GetPredictionsQuery();
        return $this->apiResponseHandler->createApiResponse($this->handleMessage($query));
    }

    public function getPrediction(): ApiResponse
    {
        $query = new GetPredictionQuery((int) $this->request->get('id'));
        return $this->apiResponseHandler->createApiResponse($this->handleMessage($query));
    }

    public function postPrediction(): ApiResponse
    {
        $command = new PostPredictionCommand($this->request);
        return $this->apiResponseHandler->createApiResponse(
            data: $this->handleMessage($command),
            httpCode: Response::HTTP_CREATED,
        );
    }

    public function patchPrediction(): ApiResponse
    {
        $command = new PatchPredictionCommand((int) $this->request->get('id'), $this->request);
        return $this->apiResponseHandler->createApiResponse($this->handleMessage($command));
    }

    public function deletePrediction(): ApiResponse
    {
        $command = new DeletePredictionCommand((int) $this->request->get('id'));
        return $this->apiResponseHandler->createApiResponse($this->handleMessage($command));
    }
}