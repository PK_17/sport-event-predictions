<?php

declare(strict_types=1);

namespace App\Controller;

use App\App\Team\Query\GetTeams\GetTeamsQuery;
use App\DTO\ApiResponse;

class TeamController extends ApiController
{
    public function getTeams(): ApiResponse
    {
        $query = new GetTeamsQuery();
        return $this->apiResponseHandler->createApiResponse($this->handleMessage($query));
    }
}