<?php

declare(strict_types=1);

namespace App\Controller;

use App\App\SportEvent\Query\GetSportEvents\GetSportEventsQuery;
use App\DTO\ApiResponse;

class SportEventController extends ApiController
{
    public function getSportEvents(): ApiResponse
    {
        $query = new GetSportEventsQuery();
        return $this->apiResponseHandler->createApiResponse($this->handleMessage($query));
    }
}