<?php

declare(strict_types=1);

namespace App\App\Prediction\Common;

use App\Entity\Prediction;
use App\Repository\PredictionRepository;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

abstract readonly class PredictionHandler
{
    public function __construct(
        protected PredictionRepository $predictionRepository,
    ) {
    }

    protected function findPrediction(int $id): Prediction
    {
        $prediction = $this->predictionRepository->find($id);
        if (!$prediction) {
            throw new NotFoundHttpException('Prediction not found.');
        }

        return $prediction;
    }
}
