<?php

declare(strict_types=1);

namespace App\App\Prediction\Command\PostPrediction;

use App\App\Prediction\Command\PostPrediction\DTO\PostPredictionDTO;
use App\Entity\Prediction;
use App\Entity\SportEvent;
use App\Entity\User;
use App\Factory\PredictionFactory;
use App\Repository\PredictionRepository;
use App\Repository\SportEventRepository;
use App\Repository\UserRepository;
use App\Validator\RequestValidator;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;

#[AsMessageHandler]
final readonly class PostPredictionHandler
{
    public function __construct(
        private EntityManagerInterface $entityManager,
        private RequestValidator $requestValidator,
        private PredictionFactory $predictionFactory,
        private PredictionRepository $predictionRepository,
        private SportEventRepository $sportEventRepository,
        private UserRepository $userRepository,
    ) {
    }

    public function __invoke(PostPredictionCommand $command): Prediction
    {
        /** @var PostPredictionDTO $dto */
        $dto = $this->requestValidator->validate($command->request, PostPredictionDTO::class);
        $this->validatePredictionUniqueness($dto);

        $sportEvent = $this->findSportEvent($dto->sportEventId);
        $user = $this->findUser($dto->userId);

        $prediction = $this->predictionFactory->create($dto, $sportEvent, $user);

        $this->entityManager->persist($prediction);
        $this->entityManager->flush();

        return $prediction;
    }

    private function findSportEvent(int $sportEventId): SportEvent
    {
        $sportEvent = $this->sportEventRepository->find($sportEventId);
        if (!$sportEvent) {
            throw new NotFoundHttpException('Sport event not found.');
        }

        return $sportEvent;
    }

    private function findUser(int $userId): User
    {
        $user = $this->userRepository->find($userId);
        if (!$user) {
            throw new NotFoundHttpException('User not found.');
        }

        return $user;
    }

    private function validatePredictionUniqueness(PostPredictionDTO $dto): void
    {
        $existingPrediction = $this->predictionRepository->findBySportEventIdAndUserId(
            $dto->sportEventId,
            $dto->userId
        );

        if ($existingPrediction) {
            throw new BadRequestHttpException('Prediction is not unique. User can only give one prediction per event.');
        }
    }
}
