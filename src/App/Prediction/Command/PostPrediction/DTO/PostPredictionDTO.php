<?php

declare(strict_types=1);

namespace App\App\Prediction\Command\PostPrediction\DTO;

use App\DTO\RequestDTO;
use App\Enum\PredictionEnum;
use Symfony\Component\Validator\Constraints as Assert;
use App\Validator as CustomAssert;

#[CustomAssert\Score]
class PostPredictionDTO extends RequestDTO
{
    #[Assert\Type(type: 'int')]
    #[Assert\NotNull]
    #[Assert\GreaterThan(0)]
    public ?int $sportEventId = null;

    #[Assert\Type(type: 'int')]
    #[Assert\NotNull]
    #[Assert\GreaterThan(0)]
    public ?int $userId = null;

    #[Assert\Type(type: 'string')]
    #[Assert\Choice(callback: [PredictionEnum::class, 'values'])]
    #[Assert\NotBlank]
    public ?string $prediction = null;

    #[Assert\Type(type: 'int')]
    #[Assert\NotNull]
    #[Assert\GreaterThanOrEqual(0)]
    #[Assert\LessThanOrEqual(100)]
    public ?int $goals = null;
}
