<?php

declare(strict_types=1);

namespace App\App\Prediction\Command\PostPrediction;

use Symfony\Component\HttpFoundation\Request;

final readonly class PostPredictionCommand
{
    public function __construct(
        public Request $request,
    ) {
    }
}
