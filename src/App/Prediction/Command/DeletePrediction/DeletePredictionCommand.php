<?php

declare(strict_types=1);

namespace App\App\Prediction\Command\DeletePrediction;

final readonly class DeletePredictionCommand
{
    public function __construct(
        public int $id,
    ) {
    }
}