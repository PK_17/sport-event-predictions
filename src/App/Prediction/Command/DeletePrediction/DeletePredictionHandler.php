<?php

declare(strict_types=1);

namespace App\App\Prediction\Command\DeletePrediction;

use App\Repository\PredictionRepository;
use Doctrine\ORM\EntityManagerInterface;
use App\App\Prediction\Common\PredictionHandler;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;

#[AsMessageHandler]
final readonly class DeletePredictionHandler extends PredictionHandler
{
    public function __construct(
        PredictionRepository $predictionRepository,
        private EntityManagerInterface $entityManager,
    ) {
        parent::__construct($predictionRepository);
    }

    public function __invoke(DeletePredictionCommand $command): bool
    {
        $prediction = $this->findPrediction($command->id);
        $this->entityManager->remove($prediction);
        $this->entityManager->flush();

        return true;
    }
}
