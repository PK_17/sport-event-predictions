<?php

declare(strict_types=1);

namespace App\App\Prediction\Command\PatchPrediction;

use App\App\Prediction\Command\PatchPrediction\DTO\PatchPredictionDTO;
use App\App\Prediction\Common\PredictionHandler;
use App\Entity\Prediction;
use App\Factory\PredictionFactory;
use App\Repository\PredictionRepository;
use App\Validator\RequestValidator;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;

#[AsMessageHandler]
final readonly class PatchPredictionHandler extends PredictionHandler
{
    public function __construct(
        PredictionRepository $predictionRepository,
        private EntityManagerInterface $entityManager,
        private RequestValidator $requestValidator,
        private PredictionFactory $predictionFactory,
    ) {
        parent::__construct($predictionRepository);
    }

    public function __invoke(PatchPredictionCommand $command): Prediction
    {
        $prediction = $this->findPrediction($command->id);
        /** @var PatchPredictionDTO $dto */
        $dto = $this->requestValidator->validate($command->request, PatchPredictionDTO::class);
        $prediction = $this->predictionFactory->update($dto, $prediction);

        $this->entityManager->persist($prediction);
        $this->entityManager->flush();

        return $prediction;
    }
}