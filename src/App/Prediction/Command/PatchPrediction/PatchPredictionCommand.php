<?php

declare(strict_types=1);

namespace App\App\Prediction\Command\PatchPrediction;

use Symfony\Component\HttpFoundation\Request;

final readonly class PatchPredictionCommand
{
    public function __construct(
        public int $id,
        public Request $request,
    ) {
    }
}