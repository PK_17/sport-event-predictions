<?php

declare(strict_types=1);

namespace App\App\Prediction\Query\GetPrediction;

use App\App\Prediction\Common\PredictionHandler;
use App\Entity\Prediction;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;

#[AsMessageHandler]
final readonly class GetPredictionHandler extends PredictionHandler
{
    public function __invoke(GetPredictionQuery $query): Prediction
    {
        return $this->findPrediction($query->id);
    }
}
