<?php

declare(strict_types=1);

namespace App\App\Prediction\Query\GetPrediction;

final readonly class GetPredictionQuery
{
    public function __construct(
        public int $id,
    ) {
    }
}