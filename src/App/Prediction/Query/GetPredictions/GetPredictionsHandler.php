<?php

declare(strict_types=1);

namespace App\App\Prediction\Query\GetPredictions;

use App\Repository\PredictionRepository;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;

#[AsMessageHandler]
final readonly class GetPredictionsHandler
{
    public function __construct(
        private PredictionRepository $predictionRepository,
    ) {
    }

    public function __invoke(GetPredictionsQuery $query): array
    {
        return $this->predictionRepository->findAll();
    }
}