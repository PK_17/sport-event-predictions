<?php

declare(strict_types=1);

namespace App\App\User\Query\GetUsers;

use App\Repository\UserRepository;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;

#[AsMessageHandler]
final readonly class GetUsersHandler
{
    public function __construct(
        private UserRepository $userRepository,
    ) {
    }

    public function __invoke(GetUsersQuery $query): array
    {
        return $this->userRepository->findAll();
    }
}
