<?php

declare(strict_types=1);

namespace App\App\User\Query\GetUserPredictions;

final readonly class GetUserPredictionsQuery
{
    public function __construct(
        public int $userId,
    ) {
    }
}