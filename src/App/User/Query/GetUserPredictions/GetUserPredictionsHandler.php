<?php

declare(strict_types=1);

namespace App\App\User\Query\GetUserPredictions;

use App\Entity\User;
use App\Repository\PredictionRepository;
use App\Repository\UserRepository;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;

#[AsMessageHandler]
final readonly class GetUserPredictionsHandler
{
    public function __construct(
        private UserRepository $userRepository,
        private PredictionRepository $predictionRepository,
    ) {
    }

    public function __invoke(GetUserPredictionsQuery $query): array
    {
        $user = $this->findUser($query->userId);
        return $this->predictionRepository->findBy(['user' => $user]);
    }

    private function findUser(int $userId): User
    {
        $user = $this->userRepository->find($userId);
        if (!$user) {
            throw new NotFoundHttpException('User not found.');
        }

        return $user;
    }
}
