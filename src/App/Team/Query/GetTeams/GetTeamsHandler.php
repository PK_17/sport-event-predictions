<?php

declare(strict_types=1);

namespace App\App\Team\Query\GetTeams;

use App\Repository\TeamRepository;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;

#[AsMessageHandler]
final readonly class GetTeamsHandler
{
    public function __construct(
        private TeamRepository $teamRepository,
    ) {
    }

    public function __invoke(GetTeamsQuery $query): array
    {
        return $this->teamRepository->findAll();
    }
}
