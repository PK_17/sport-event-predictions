<?php

declare(strict_types=1);

namespace App\App\SportEvent\Query\GetSportEvents;

use App\Repository\SportEventRepository;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;

#[AsMessageHandler]
final readonly class GetSportEventsHandler
{
    public function __construct(
        private SportEventRepository $sportEventRepository,
    ) {
    }

    public function __invoke(GetSportEventsQuery $query): array
    {
        return $this->sportEventRepository->findAll();
    }
}