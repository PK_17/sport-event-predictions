<?php

declare(strict_types=1);

namespace App\Enum;

enum PredictionEnum: string
{
    case HOME_WIN = '1';
    case DRAW = 'X';
    case AWAY_WIN = '2';

    public static function values(): array
    {
        return array_column(static::cases(), 'value');
    }
}
