<?php

declare(strict_types=1);

namespace App\DTO;

use Symfony\Component\HttpFoundation\JsonResponse;

final class ApiResponse extends JsonResponse
{
    public function __construct(mixed $data, int $httpCode)
    {
        parent::__construct($data, $httpCode, [], true);
    }
}