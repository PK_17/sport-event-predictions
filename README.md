# sport-event-predictions

## Manual

* git clone https://gitlab.com/PK_17/sport-event-predictions.git
* Run docker `docker-compose up --build` (add `-d` to run it in detach mode if necessary)
* Log onto the container `docker exec -it sep_api bash` and do the following:
    * `composer install`
    *  NOTICE!: database sep_db should already exist at this point, but in the case it does not use the following command to create it: `php bin/console d:d:c`
    * `php bin/console d:m:m -n` - execute migrations with no interaction
    * `php bin/console d:f:l -n` - load fixtures with no interaction
* Execute PhpUnit tests:
    * `php bin/console d:d:c --env=test` - create database sep_db_test
    * `php bin/console d:m:m -n --env=test` - execute migrations with no interaction
    * `php bin/console d:f:l -n --env=test` - load fixtures with no interaction
    * `php bin/phpunit` - run controller tests (13)
* After that we can start using sport-event-predictions api. Description of all endpoints can be found below.
* All data necessary to connect to both databases is present in `.env` file

## Endpoints description

* Endpoints are divided into four groups, because we have four entities in the system - SportEvent, User, Team and Prediction
* <b>SportEvent:</b>
  * `GET/api/sport-events` - get a list of predefined events (six matches)
* <b>User:</b>
  * `GET/api/users` - get a list of predefined users (10)
  * `GET/api/user/{id}/predictions` - get a list of all predictions given by a certain user (id = userId) 
* <b>Team:</b>
  * `GET/api/teams` - get a list of predefined teams (4)
* <b>Predictions:</b>
  * `GET/api/predictions` - predictions are not predefined, thus user has to add new ones manually via POST endpoint
  * `GET/api/prediction/{id}` - get one prediction by its id
  * `POST/api/prediction` - add new prediction to the system. Request body example:
    ```
    {
      "userId": 3,
      "sportEventId": 2,
      "prediction": "1",
      "goals": 1
    }
    ```
    user can add only one prediction per event and if user bets that the result will be a draw, he/she has to provide an even number of goals. A win prediction has to have an odd number of goals. 
  * `PATCH/api/prediction/{id}` - allows user to modify his/her prediction (id = predictionId). Request body example:
    ```
    {
      "prediction": "1",
      "goals": 1
    }
    ```
    goal rule for draw/win is still valid for this endpoint 
  * `DELETE/api/prediction/{id}` - removes prediction from the system

## Additional information

A postman collection with all listed endpoints can be found in `resources` folder.
