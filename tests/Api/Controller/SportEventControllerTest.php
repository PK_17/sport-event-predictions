<?php

declare(strict_types=1);

namespace App\Tests\Api\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class SportEventControllerTest extends ApiControllerTest
{
    private const GET_SPORT_EVENTS_URL = '/api/sport-events';

    public function testGetSportEvents(): void
    {
        $this->client->request(Request::METHOD_GET, self::GET_SPORT_EVENTS_URL);
        $response = $this->client->getResponse();
        self::assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }
}
