<?php

declare(strict_types=1);

namespace App\Tests\Api\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class PredictionControllerTest extends ApiControllerTest
{
    private const POST_PREDICTION_URL = '/api/prediction';
    private const PATCH_PREDICTION_URL = '/api/prediction/%d';
    private const DELETE_PREDICTION_URL = '/api/prediction/%d';
    private const GET_PREDICTION_URL = '/api/prediction/%d';
    private const GET_PREDICTIONS_URL = '/api/predictions';
    private const TEST_USER_ID = 1;
    private const TEST_SPORT_EVENT_ID = 1;
    private const NON_EXISTENT_PREDICTION_ID = 999;

    public function testGetPredictions(): void
    {
        $this->client->request(Request::METHOD_GET, self::GET_PREDICTIONS_URL);
        $response = $this->client->getResponse();
        self::assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }

    public function testPostValidPrediction(): int
    {
        $body = [
            'userId' => self::TEST_USER_ID,
            'sportEventId' => self::TEST_SPORT_EVENT_ID,
            'prediction' => '1',
            'goals' => 3,
        ];

        $this->client->request(Request::METHOD_POST, self::POST_PREDICTION_URL, content: json_encode($body));
        $response = $this->client->getResponse();
        self::assertEquals(Response::HTTP_CREATED, $response->getStatusCode());
        $response = $this->getResponseAsArray($response);
        return $response['id'];
    }

    public function testPostInvalidPrediction(): void
    {
        $body = [ // not unique user and sport event pair
            'userId' => self::TEST_USER_ID,
            'sportEventId' => self::TEST_SPORT_EVENT_ID,
            'prediction' => '1',
            'goals' => 3,
        ];

        $this->client->request(Request::METHOD_POST, self::POST_PREDICTION_URL, content: json_encode($body));
        $response = $this->client->getResponse();
        self::assertEquals(Response::HTTP_BAD_REQUEST, $response->getStatusCode());
    }

    /**
     * @depends testPostValidPrediction
     */
    public function testPatchValidPrediction(int $predictionId): void
    {
        $url = sprintf(self::PATCH_PREDICTION_URL, $predictionId);
        $body = [
            'prediction' => 'X',
            'goals' => 2,
        ];

        $this->client->request(Request::METHOD_PATCH, $url, content: json_encode($body));
        $response = $this->client->getResponse();
        self::assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }

    /**
     * @depends testPostValidPrediction
     */
    public function testPatchInvalidPrediction(int $predictionId): void
    {
        $url = sprintf(self::PATCH_PREDICTION_URL, $predictionId);
        $body = [ // draw with an event number of goals is incorrect
            'prediction' => 'X',
            'goals' => 3,
        ];

        $this->client->request(Request::METHOD_PATCH, $url, content: json_encode($body));
        $response = $this->client->getResponse();
        self::assertEquals(Response::HTTP_BAD_REQUEST, $response->getStatusCode());
    }

    public function testGetNonExistentPrediction(): void
    {
        $url = sprintf(self::GET_PREDICTION_URL, self::NON_EXISTENT_PREDICTION_ID);
        $this->client->request(Request::METHOD_GET, $url);
        $response = $this->client->getResponse();
        self::assertEquals(Response::HTTP_NOT_FOUND, $response->getStatusCode());
    }

    /**
     * @depends testPostValidPrediction
     */
    public function testGetExistingPrediction(int $predictionId): void
    {
        $url = sprintf(self::GET_PREDICTION_URL, $predictionId);
        $this->client->request(Request::METHOD_GET, $url);
        $response = $this->client->getResponse();
        self::assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }

    public function testDeleteNonExistentPrediction(): void
    {
        $url = sprintf(self::DELETE_PREDICTION_URL, self::NON_EXISTENT_PREDICTION_ID);
        $this->client->request(Request::METHOD_DELETE, $url);
        $response = $this->client->getResponse();
        self::assertEquals(Response::HTTP_NOT_FOUND, $response->getStatusCode());
    }

    /**
     * @depends testPostValidPrediction
     */
    public function testDeleteExistingPrediction(int $predictionId): void
    {
        $url = sprintf(self::DELETE_PREDICTION_URL, $predictionId);
        $this->client->request(Request::METHOD_DELETE, $url);
        $response = $this->client->getResponse();
        self::assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }
}
