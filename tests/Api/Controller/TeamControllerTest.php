<?php

declare(strict_types=1);

namespace App\Tests\Api\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class TeamControllerTest extends ApiControllerTest
{
    private const GET_TEAMS_URL = '/api/teams';

    public function testGetTeams(): void
    {
        $this->client->request(Request::METHOD_GET, self::GET_TEAMS_URL);
        $response = $this->client->getResponse();
        self::assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }
}
