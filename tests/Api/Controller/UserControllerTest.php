<?php

declare(strict_types=1);

namespace App\Tests\Api\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class UserControllerTest extends ApiControllerTest
{
    private const GET_USERS_URL = '/api/users';
    private const GET_USER_PREDICTIONS_URL = '/api/user/%d/predictions';
    private const TEST_USER_ID = 1;

    public function testGetUsers(): void
    {
        $this->client->request(Request::METHOD_GET, self::GET_USERS_URL);
        $response = $this->client->getResponse();
        self::assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }

    public function testGetUserPredictions(): void
    {
        $url = sprintf(self::GET_USER_PREDICTIONS_URL, self::TEST_USER_ID);
        $this->client->request(Request::METHOD_GET, $url);
        $response = $this->client->getResponse();
        self::assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }
}
